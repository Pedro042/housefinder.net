﻿using System;

namespace Housefinder.Data
{
    public class Property
    {
        //Property ID
        public int ID { get; set; }

        // Agent-chosen property title
        public string Title { get; set; }

        // The Type of the propery: House, Apartment, Rural - SHOULD THIS BE AN ENUM?
        public string PropertyType { get; set; }

        // Suburb
        public string Suburb { get; set; }

        // PostCode - SHOULD THIS BE INT? or STRING/ENUM?
        public int PostCode { get; set; }

        // Street Address of the property
        public string StreetAddress { get; set; }

        // Number of Bedrooms
        public int Bedrooms { get; set; }

        // Number of Bathrooms
        public int Bathrooms { get; set; }

        // Number of Car Parking spaces
        public int Cars { get; set; }

        // Price (Range) of the property - SHOULD THIS BE MONEY? Currently can be a range though
        public string Price { get; set; }

        // Main Body describing the property
        public string Body { get; set; }

        // Summary of main features
        public string Features { get; set; }

        // List of images for Property - SHOULD END UP AN ARRAY?
        public string ImageURLs { get; set; }

        // System-generated date stamp when record was created
        public DateTime DateStamp { get; set; }
    }
}
